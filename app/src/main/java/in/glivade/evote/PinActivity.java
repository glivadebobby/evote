package in.glivade.evote;

import static in.glivade.evote.app.Activity.launchClearStack;
import static in.glivade.evote.app.Activity.launchClearStackBundle;
import static in.glivade.evote.app.Api.KEY_ERROR;
import static in.glivade.evote.app.Api.KEY_MESSAGE;
import static in.glivade.evote.app.Api.KEY_STATUS;
import static in.glivade.evote.app.Api.KEY_TAG;
import static in.glivade.evote.app.Api.KEY_VOTER_ID;
import static in.glivade.evote.app.Api.URL;
import static in.glivade.evote.app.Api.VALUE_STATUS;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.glivade.evote.app.AppController;
import in.glivade.evote.app.PreferenceManager;
import in.glivade.evote.app.ToastBuilder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PinActivity extends AppCompatActivity
        implements View.OnClickListener {

    private Context mContext;
    private EditText mEditTextPin;
    private Button mButtonSubmit;
    private AnimationDrawable mAnimationDrawable;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mButtonSubmit) {
            processLogin();
        }
    }

    private void initObjects() {
        mEditTextPin = (TextInputEditText) findViewById(R.id.input_pin);
        mButtonSubmit = (Button) findViewById(R.id.btn_submit);

        mContext = this;
        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.pin).getBackground();
        mPreferenceManager = new PreferenceManager(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonSubmit.setOnClickListener(this);
    }

    private void processLogin() {
        String pin = mEditTextPin.getText().toString().trim();
        if (validateInput(pin)) {
            showProgressDialog("Verifying..");
            updateStatus();
        }
    }

    private boolean validateInput(String pin) {
        if (TextUtils.isEmpty(pin)) {
            mEditTextPin.requestFocus();
            mEditTextPin.setError(getString(R.string.error_empty));
            return false;
        } else if (!pin.equalsIgnoreCase(mPreferenceManager.getPin())) {
            ToastBuilder.build(mContext, "Pin verification failed");
            mPreferenceManager.clearUser();
            launchClearStack(mContext, SplashActivity.class);
            return false;
        }
        return true;
    }

    private void updateStatus() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleStatusResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                ToastBuilder.build(mContext, error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_STATUS);
                params.put(KEY_VOTER_ID, mPreferenceManager.getVoterId());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "status");
    }

    private void handleStatusResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);
            String message = jsonObject.getString(KEY_MESSAGE);
            ToastBuilder.build(mContext, message);

            if (error == 0) {
                mPreferenceManager.clearUser();

                String status = jsonObject.getString(KEY_STATUS);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_STATUS, status);
                launchClearStackBundle(mContext, MainActivity.class, bundle);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
