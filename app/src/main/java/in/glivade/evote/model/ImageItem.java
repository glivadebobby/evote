package in.glivade.evote.model;

/**
 * Created by Bobby on 26-03-2017
 */

public class ImageItem {

    private int mId;
    private byte[] mImage;

    public ImageItem(int id, byte[] image) {
        mId = id;
        mImage = image;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public byte[] getImage() {
        return mImage;
    }

    public void setImage(byte[] image) {
        mImage = image;
    }
}
