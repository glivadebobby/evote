package in.glivade.evote.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.glivade.evote.R;

/**
 * Created by Bobby on 26-03-2017
 */

public class ImageHolder extends RecyclerView.ViewHolder {

    private ImageView mImageView;
    private View mViewSelected;
    private TextView mTextViewSelected;

    public ImageHolder(View itemView) {
        super(itemView);
        mImageView = (ImageView) itemView.findViewById(R.id.img);
        mViewSelected = itemView.findViewById(R.id.selected);
        mTextViewSelected = (TextView) itemView.findViewById(R.id.txt_selected);
    }

    public ImageView getImageView() {
        return mImageView;
    }

    public View getViewSelected() {
        return mViewSelected;
    }

    public TextView getTextViewSelected() {
        return mTextViewSelected;
    }
}
