package in.glivade.evote;

import static in.glivade.evote.app.Activity.launch;
import static in.glivade.evote.app.Activity.launchClearStack;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import in.glivade.evote.adapter.ImageAdapter;
import in.glivade.evote.app.PreferenceManager;
import in.glivade.evote.app.ToastBuilder;
import in.glivade.evote.model.ImageItem;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ImageActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final int[] IMAGES =
            {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5,
                    R.drawable.img6, R.drawable.img7, R.drawable.img8, R.drawable.img9};
    private Context mContext;
    private RecyclerView mRecyclerView;
    private Button mButtonSubmit;
    private AnimationDrawable mAnimationDrawable;
    private List<ImageItem> mImageItemList;
    private ImageAdapter mAdapter;
    private PreferenceManager mPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        initObjects();
        initCallbacks();
        initRecyclerView();
        populateImages();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonSubmit) {
            if (mAdapter.isSuccess()) {
                ToastBuilder.build(mContext, "Success");
                launch(mContext, VoiceActivity.class);
            } else {
                ToastBuilder.build(mContext, "Image verification failed");
                mPreferenceManager.clearUser();
                launchClearStack(mContext, SplashActivity.class);
            }
        }
    }

    private void initObjects() {
        mRecyclerView = (RecyclerView) findViewById(R.id.img_list);
        mButtonSubmit = (Button) findViewById(R.id.btn_submit);

        mContext = this;
        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.image).getBackground();
        mImageItemList = new ArrayList<>();
        mAdapter = new ImageAdapter(mContext, mImageItemList);
        mPreferenceManager = new PreferenceManager(mContext);
    }

    private void initCallbacks() {
        mButtonSubmit.setOnClickListener(this);
    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
    }

    private void populateImages() {
        for (int i = 1; i <= 9; i++) {
            mImageItemList.add(new ImageItem(i, convertToBitmap(IMAGES[i - 1])));
        }

        Collections.shuffle(mImageItemList);
        int image1 = mImageItemList.get(0).getId() - 1;
        int image2 = mImageItemList.get(1).getId() - 1;

        mImageItemList.get(image1).setImage(
                Base64.decode(mPreferenceManager.getImage1(), Base64.DEFAULT));
        mImageItemList.get(image2).setImage(
                Base64.decode(mPreferenceManager.getImage2(), Base64.DEFAULT));

        mAdapter.setImage1Id(mImageItemList.get(image1).getId());
        mAdapter.setImage2Id(mImageItemList.get(image2).getId());
        mAdapter.notifyDataSetChanged();
    }

    private byte[] convertToBitmap(int image) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), image);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }
}
