package in.glivade.evote;


import static in.glivade.evote.app.Activity.launch;
import static in.glivade.evote.app.Api.KEY_ERROR;
import static in.glivade.evote.app.Api.KEY_IMAGE_1;
import static in.glivade.evote.app.Api.KEY_IMAGE_2;
import static in.glivade.evote.app.Api.KEY_MAC_ID;
import static in.glivade.evote.app.Api.KEY_MESSAGE;
import static in.glivade.evote.app.Api.KEY_PIN;
import static in.glivade.evote.app.Api.KEY_TAG;
import static in.glivade.evote.app.Api.KEY_VOICE_TEXT;
import static in.glivade.evote.app.Api.KEY_VOTER_ID;
import static in.glivade.evote.app.Api.URL;
import static in.glivade.evote.app.Api.VALUE_LOGIN;
import static in.glivade.evote.app.Util.getMacAddress;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.glivade.evote.app.AppController;
import in.glivade.evote.app.PreferenceManager;
import in.glivade.evote.app.ToastBuilder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity
        implements View.OnClickListener {

    private Context mContext;
    private EditText mEditTextVoterId;
    private Button mButtonLogin;
    private AnimationDrawable mAnimationDrawable;
    private PreferenceManager mPreferenceManager;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mButtonLogin) {
            processLogin();
        }
    }

    private void initObjects() {
        mEditTextVoterId = (TextInputEditText) findViewById(R.id.input_voter_id);
        mButtonLogin = (Button) findViewById(R.id.btn_login);

        mContext = this;
        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.login).getBackground();
        mPreferenceManager = new PreferenceManager(mContext);
        mProgressDialog = new ProgressDialog(mContext);
    }

    private void initCallbacks() {
        mButtonLogin.setOnClickListener(this);
    }

    private void processLogin() {
        String voterId = mEditTextVoterId.getText().toString().trim();
        if (validateInput(voterId)) {
            showProgressDialog("Logging in..");
            loginUser(voterId);
        }
    }

    private boolean validateInput(String voterId) {
        if (TextUtils.isEmpty(voterId)) {
            mEditTextVoterId.requestFocus();
            mEditTextVoterId.setError(getString(R.string.error_empty));
            return false;
        }
        return true;
    }

    private void loginUser(final String voterId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        handleSignInResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                hideProgressDialog();
                ToastBuilder.build(mContext, error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_TAG, VALUE_LOGIN);
                params.put(KEY_VOTER_ID, voterId);
                params.put(KEY_MAC_ID, getMacAddress());
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, "login");
    }

    private void handleSignInResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            int error = jsonObject.getInt(KEY_ERROR);
            String message = jsonObject.getString(KEY_MESSAGE);
            ToastBuilder.build(mContext, message);

            if (error == 0) {
                mPreferenceManager.setVoterId(jsonObject.getString(KEY_VOTER_ID));
                mPreferenceManager.setImage1(jsonObject.getString(KEY_IMAGE_1));
                mPreferenceManager.setImage2(jsonObject.getString(KEY_IMAGE_2));
                mPreferenceManager.setVoiceText(jsonObject.getString(KEY_VOICE_TEXT));
                mPreferenceManager.setPin(jsonObject.getString(KEY_PIN));

                launch(mContext, ImageActivity.class);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
