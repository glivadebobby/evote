package in.glivade.evote.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import in.glivade.evote.R;
import in.glivade.evote.holder.ImageHolder;
import in.glivade.evote.model.ImageItem;

/**
 * Created by Bobby on 26-03-2017
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageHolder> {

    private static final int RESET = -1;
    private Context mContext;
    private List<ImageItem> mImageItemList;
    private int mImage1Id, mImage2Id, mSelectedImage1Id, mSelectedImage2Id, mLastSelectedId;

    public ImageAdapter(Context context, List<ImageItem> imageItemList) {
        mContext = context;
        mImageItemList = imageItemList;
    }

    public void setImage1Id(int image1Id) {
        mImage1Id = image1Id;
    }

    public void setImage2Id(int image2Id) {
        mImage2Id = image2Id;
    }

    public boolean isSuccess() {
        return mImage1Id == mSelectedImage1Id && mImage2Id == mSelectedImage2Id;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_img, parent, false));
    }

    @Override
    public void onBindViewHolder(final ImageHolder holder, int position) {
        ImageItem imageItem = mImageItemList.get(position);
        int id = imageItem.getId();
        byte[] image = imageItem.getImage();

        if (id == mSelectedImage1Id) {
            holder.getViewSelected().setVisibility(View.VISIBLE);
            holder.getTextViewSelected().setVisibility(View.VISIBLE);
            holder.getTextViewSelected().setText(mContext.getString(R.string.txt_1));
        } else if (id == mSelectedImage2Id) {
            holder.getViewSelected().setVisibility(View.VISIBLE);
            holder.getTextViewSelected().setVisibility(View.VISIBLE);
            holder.getTextViewSelected().setText(mContext.getString(R.string.txt_2));
        } else {
            holder.getViewSelected().setVisibility(View.GONE);
            holder.getTextViewSelected().setVisibility(View.GONE);
        }

        Glide.with(mContext).load(image).asBitmap().dontAnimate().into(holder.getImageView());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invalidateSelection(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageItemList.size();
    }

    private void invalidateSelection(int position) {
        ImageItem imageItem = mImageItemList.get(position);
        if (imageItem.getId() == mSelectedImage1Id) {
            mSelectedImage1Id = RESET;
        } else if (imageItem.getId() == mSelectedImage2Id) {
            mSelectedImage2Id = RESET;
        } else if (mLastSelectedId == 0) {
            mSelectedImage1Id = imageItem.getId();
            mLastSelectedId = 1;
        } else if (mLastSelectedId == 1) {
            mSelectedImage2Id = imageItem.getId();
            mLastSelectedId = RESET;
        } else {
            mSelectedImage1Id = RESET;
            mSelectedImage2Id = RESET;
            mLastSelectedId = 0;
        }
        notifyDataSetChanged();
    }
}
