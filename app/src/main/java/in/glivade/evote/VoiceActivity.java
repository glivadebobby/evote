package in.glivade.evote;

import static in.glivade.evote.app.Activity.launch;
import static in.glivade.evote.app.Activity.launchClearStack;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Locale;

import in.glivade.evote.app.PreferenceManager;
import in.glivade.evote.app.ToastBuilder;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class VoiceActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final int REQUEST_VOICE_INPUT = 4;
    private Context mContext;
    private ImageView mImageViewMic;
    private AnimationDrawable mAnimationDrawable;
    private PreferenceManager mPreferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAnimationDrawable != null && !mAnimationDrawable.isRunning()) {
            mAnimationDrawable.start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAnimationDrawable != null && mAnimationDrawable.isRunning()) {
            mAnimationDrawable.stop();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_VOICE_INPUT && resultCode == RESULT_OK && null != data) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (!result.isEmpty()) {
                String speechData = result.get(0);
                if (speechData.equalsIgnoreCase(mPreferenceManager.getVoiceText())) {
                    ToastBuilder.build(mContext, "Success");
                    launch(mContext, PinActivity.class);
                } else {
                    ToastBuilder.build(mContext, "Voice verification failed");
                    mPreferenceManager.clearUser();
                    launchClearStack(mContext, SplashActivity.class);
                }
            } else {
                ToastBuilder.build(mContext, "Sorry, couldn't recognize your words");
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == mImageViewMic) {
            promptSpeechInput();
        }
    }

    private void initObjects() {
        mImageViewMic = (ImageView) findViewById(R.id.img_mic);

        mContext = this;
        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.voice).getBackground();
        mPreferenceManager = new PreferenceManager(mContext);
    }

    private void initCallbacks() {
        mImageViewMic.setOnClickListener(this);
    }

    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        startActivityForResult(intent, REQUEST_VOICE_INPUT);
    }
}
