package in.glivade.evote;

import static in.glivade.evote.app.Activity.launch;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initObjects();
        startLoginActivity();
    }

    private void initObjects() {
        mContext = this;
    }

    private void startLoginActivity() {
        launch(mContext, LoginActivity.class);
    }
}
