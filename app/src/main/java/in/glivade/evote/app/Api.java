package in.glivade.evote.app;

/**
 * Created by Bobby on 26-01-2017
 */

public class Api {
    /**
     * Api Keys
     */
    public static final String KEY_TAG = "tag";
    public static final String KEY_VOTER_ID = "voterid";
    public static final String KEY_MAC_ID = "macid";
    public static final String KEY_IMAGE_1 = "image1";
    public static final String KEY_IMAGE_2 = "image2";
    public static final String KEY_VOICE_TEXT = "voicetext";
    public static final String KEY_PIN = "pin";
    public static final String KEY_ERROR = "error";
    public static final String KEY_MESSAGE = "message";
    public static final String KEY_STATUS = "status";
    /**
     * Api Values
     */
    public static final String VALUE_LOGIN = "login";
    public static final String VALUE_STATUS = "status";
    /**
     * Api URLS
     */
    public static final String URL = "http://evote.glivade.in/index.php";
}
