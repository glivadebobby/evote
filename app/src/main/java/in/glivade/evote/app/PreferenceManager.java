package in.glivade.evote.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by Bobby on 23-01-2017
 */

public class PreferenceManager {

    private static final String PREF_USER = "user";
    private static final String VOTER_ID = "voter_id";
    private static final String IMAGE_1 = "image_1";
    private static final String IMAGE_2 = "image_2";
    private static final String VOICE_TEXT = "voice_text";
    private static final String PIN = "pin";
    private SharedPreferences mPreferencesUser;

    public PreferenceManager(Context context) {
        mPreferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
    }

    public String getVoterId() {
        return mPreferencesUser.getString(encode(VOTER_ID), null);
    }

    public void setVoterId(String voterId) {
        mPreferencesUser.edit().putString(encode(VOTER_ID), voterId).apply();
    }

    public String getImage1() {
        return mPreferencesUser.getString(encode(IMAGE_1), null);
    }

    public void setImage1(String image1) {
        mPreferencesUser.edit().putString(encode(IMAGE_1), image1).apply();
    }

    public String getImage2() {
        return mPreferencesUser.getString(encode(IMAGE_2), null);
    }

    public void setImage2(String image2) {
        mPreferencesUser.edit().putString(encode(IMAGE_2), image2).apply();
    }

    public String getVoiceText() {
        return mPreferencesUser.getString(encode(VOICE_TEXT), null);
    }

    public void setVoiceText(String voiceText) {
        mPreferencesUser.edit().putString(encode(VOICE_TEXT), voiceText).apply();
    }

    public String getPin() {
        return mPreferencesUser.getString(encode(PIN), null);
    }

    public void setPin(String pin) {
        mPreferencesUser.edit().putString(encode(PIN), pin).apply();
    }

    public void clearUser() {
        mPreferencesUser.edit().clear().apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }
}
